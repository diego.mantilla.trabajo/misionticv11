package com.example.misionticv11;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.misionticv11.dialogfragment.BotonOpcion;
import com.example.misionticv11.dialogfragment.EjemploDialogFragment;

public class semana4_ejemplo3_dialogfragment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semana4_ejemplo3_dialogfragment);

        Button btnVer = (Button) findViewById(R.id.semana4_ejemplo3_btnVer);
        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new EjemploDialogFragment(
                        "Titulo1",
                        "Este es el cuerpo del mensaje",
                        new BotonOpcion(
                                "Aceptar",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                }),
                        new BotonOpcion(
                                "Cancelar",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                }
                        )).show(getSupportFragmentManager(), "DialogFragment");
            }
        });

    }
}