package com.example.misionticv11.adapters;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.example.misionticv11.R;
import com.example.misionticv11.modelos.Sitio;

import java.util.ArrayList;

public class EjemploSpinnerAdapter implements SpinnerAdapter {

    private final ArrayList<Sitio> datos;

    public EjemploSpinnerAdapter(ArrayList<Sitio> datos) {
        this.datos = datos;
    }

    @Override
    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        TwoLineListItem tv = (TwoLineListItem) LayoutInflater.from(viewGroup.getContext()).inflate(android.R.layout.simple_list_item_2, viewGroup, false);
        tv.getText1().setText(datos.get(i).getNombre());
        tv.getText2().setText(datos.get(i).getDescripcion());
        return tv;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int i) {
        return datos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TwoLineListItem tv = (TwoLineListItem) LayoutInflater.from(viewGroup.getContext()).inflate(android.R.layout.simple_list_item_2, viewGroup, false);
        tv.getText1().setText(datos.get(i).getNombre());
        tv.getText2().setText(datos.get(i).getDescripcion());
        return tv;
    }

    @Override
    public int getItemViewType(int i) {
        return i;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
