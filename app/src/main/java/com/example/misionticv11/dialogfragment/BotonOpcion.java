package com.example.misionticv11.dialogfragment;

import android.content.DialogInterface;

public class BotonOpcion{

    public String tituloBoton;
    public DialogInterface.OnClickListener eventoBoton;

    public BotonOpcion(String tituloBoton, DialogInterface.OnClickListener eventoBoton) {
        this.tituloBoton = tituloBoton;
        this.eventoBoton = eventoBoton;
    }
}
