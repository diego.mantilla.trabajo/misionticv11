package com.example.misionticv11.dialogfragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class EjemploDialogFragment extends DialogFragment {

    private String titulo;
    private String mensaje;
    private BotonOpcion opcionSi;
    private BotonOpcion opcionNo;

    public EjemploDialogFragment(String titulo, String mensaje) {
        this.titulo = titulo;
        this.mensaje = mensaje;
    }

    public EjemploDialogFragment(String titulo, String mensaje, BotonOpcion opcionSi) {
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.opcionSi = opcionSi;
    }

    public EjemploDialogFragment(String titulo, String mensaje, BotonOpcion opcionSi, BotonOpcion opcionNo) {
        this.titulo = titulo;
        this.mensaje = mensaje;
        this.opcionSi = opcionSi;
        this.opcionNo = opcionNo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder msj = new AlertDialog.Builder(requireContext());
        msj.setTitle(this.titulo);
        msj.setMessage(this.mensaje);
        if(this.opcionSi!=null)
            msj.setPositiveButton(this.opcionSi.tituloBoton, this.opcionSi.eventoBoton);
        if(this.opcionNo!=null)
            msj.setNegativeButton(this.opcionNo.tituloBoton, this.opcionNo.eventoBoton);

        return msj.create();
    }
}
