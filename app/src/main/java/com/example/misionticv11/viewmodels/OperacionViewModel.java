package com.example.misionticv11.viewmodels;

import com.example.misionticv11.modelos.Operacion;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class OperacionViewModel extends ViewModel {
    private MutableLiveData<Operacion> operacion = new MutableLiveData<>();

    public MutableLiveData<Operacion> getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion.setValue(operacion);
    }

}
