package com.example.misionticv11.viewmodels;

import com.example.misionticv11.modelos.Sitio;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SitioViewModel extends ViewModel {
    private MutableLiveData<Sitio> sitio = new MutableLiveData<>();

    public MutableLiveData<Sitio> getSitio() {
        return sitio;
    }

    public void setSitio(Sitio sitio) {
        this.sitio.setValue(sitio);
    }
}
