package com.example.misionticv11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.misionticv11.ado.UsuarioADO;
import com.example.misionticv11.clases.Mensajes;
import com.example.misionticv11.modelos.Usuario;

public class semana3_usuario_insertar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semana3_usuario_insertar);

        EditText txtNombres = (EditText) findViewById(R.id.usuario_insertar_txtNombres);
        EditText txtApellidos = (EditText) findViewById(R.id.usuario_insertar_txtApellidos);
        EditText txtEmail = (EditText) findViewById(R.id.usuario_insertar_txtEmail);
        EditText txtClave = (EditText) findViewById(R.id.usuario_insertar_txtClave);
        Button btnGuardar = (Button) findViewById(R.id.usuario_insertar_btnGuardar);
        Button btnCancelar = (Button) findViewById(R.id.usuario_insertar_btnCancelar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Usuario us = crearUsuario(txtNombres, txtApellidos, txtEmail, txtClave);
                UsuarioADO usADO = new UsuarioADO(view.getContext());
                long id = usADO.insertar(us);

                if(id>0)
                    new Mensajes(view.getContext()).alert("Registro guardado", "Se ha insertado el registro corretamente con el id " + String.valueOf(id));
                else
                    new Mensajes(view.getContext()).alert("Error", "Se ha producido un error al intentar insertar el registro");
            }
        });



    }

    private Usuario crearUsuario(EditText nombre, EditText apellido, EditText email, EditText clave)
    {
        Usuario us = new Usuario();

        String nombreUsuario = nombre.getText().toString();
        String apellidoUsuario = apellido.getText().toString();
        String emailUsuario = email.getText().toString();
        String claveUsuario = clave.getText().toString();

        us.setNombres(nombreUsuario);
        us.setApellidos(apellidoUsuario);
        us.setEmail(emailUsuario);
        us.setClave(claveUsuario);

        return us;
    }
}