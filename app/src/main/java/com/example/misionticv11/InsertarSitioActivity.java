package com.example.misionticv11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.misionticv11.ado.SitioADO;
import com.example.misionticv11.ado.UsuarioADO;
import com.example.misionticv11.clases.Mensajes;
import com.example.misionticv11.modelos.Sitio;
import com.example.misionticv11.modelos.Usuario;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

public class InsertarSitioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.Theme_Misionticv11_sinbarra);
        setContentView(R.layout.activity_insertar_sitio);

        EditText txtNombre = (EditText) findViewById(R.id.sitio_insertar_txtNombre);
        EditText txtDescripcion = (EditText) findViewById(R.id.sitio_insertar_txtDescripcion);
        EditText txtLatitud = (EditText) findViewById(R.id.sitio_insertar_txtLatitud);
        EditText txtLongitud = (EditText) findViewById(R.id.sitio_insertar_txtLongitud);
        Spinner spnTipo = (Spinner) findViewById(R.id.sitio_insertar_spnTipo);
        Button btnVolver = (Button) findViewById(R.id.sitio_insertar_btnVolver);
        Button btnGuardar = (Button) findViewById(R.id.sitio_insertar_btnGuardar);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                double lat = 0;
                double lon = 0;
                String nombre = txtNombre.getText().toString();
                String descripcion = txtDescripcion.getText().toString();
                String tipo = (String) spnTipo.getSelectedItem();

                try {
                    lat = Double.parseDouble(txtLatitud.getText().toString());
                    lon = Double.parseDouble(txtLongitud.getText().toString());
                }
                catch (Exception ex)
                {}

                if (validarCamposVacios(nombre, descripcion, lat, lon)) {
                    Sitio registro = new Sitio();
                    registro.setNombre(nombre);
                    registro.setDescripcion(descripcion);
                    registro.setLatitud(lat);
                    registro.setLongitud(lon);
                    registro.setTipo(tipo);

                    SitioADO db = new SitioADO(view.getContext());
                    long id = db.insertar(registro);
                    registro.setId((int) id);

                    //Conexion con Firebase.
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    database.getReference().child("Sitio").child(String.valueOf(id)).setValue(registro);


                    if (id > 0)
                        new Mensajes(view.getContext()).alert("Registro guardado", "Se ha guardado el registro " + String.valueOf(id) + " satisfactoriamente!!.");
                    else
                        new Mensajes(view.getContext()).alert("Error", "Ha ocurrido un error al intentar guardar el registro.");
                    db.listar();
                }
                else {
                    new Mensajes(view.getContext()).alert("Advertencia", "Se han encontrado campos vacios, por favor digitelos.");
                }
            }
        });



    }

    public static boolean validarCamposVacios(String nombre, String descripcion, double latitud, double longitud)
    {
        boolean camposValidados = false;

        //Forma larga
/*        if(!nombre.isEmpty())
            if(!descripcion.isEmpty())
                if(latitud>0)
                    if(longitud>0)
                        camposValidados=true;*/

        //Forma corta
        if(!nombre.isEmpty() && !descripcion.isEmpty() && latitud>0 && longitud>0)
            camposValidados=true;

        return camposValidados;
    }
}