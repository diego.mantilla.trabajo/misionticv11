package com.example.misionticv11.ado;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.misionticv11.clases.SqliteConex;
import com.example.misionticv11.modelos.Usuario;

import androidx.annotation.Nullable;

public class UsuarioADO extends SqliteConex {

    private Context c;

    public UsuarioADO(@Nullable Context contexto)
    {
        super(contexto);
        this.c = contexto;
    }

    public long insertar(Usuario us)
    {
        long id = 0;

        try
        {
            SqliteConex dbConex = new SqliteConex(this.c);
            SQLiteDatabase db = dbConex.getWritableDatabase();

            ContentValues valores = new ContentValues();
            valores.put("nombres", us.getNombres());
            valores.put("apellidos", us.getApellidos());
            valores.put("email", us.getEmail());
            valores.put("clave", us.getClave());

            id = db.insert("usuarios", null, valores);
        }
        catch (Exception ex)
        {

        }

        return id;
    }

    public boolean verificarUsuario(String usuario, String clave)
    {
        boolean existe = false;

        String usuarioDefecto = "usuario";
        String claveDefecto = "123";

        if(usuario.equals(usuarioDefecto) && clave.equals(claveDefecto))
            existe=true;

        return existe;
    }



}
