package com.example.misionticv11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class semana4_arrayadapter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semana4_arrayadapter);

        Spinner spEjemplo = (Spinner) findViewById(R.id.semana4_spinner_array);
        TextView txtResultado = (TextView) findViewById(R.id.semana4_resultado_arrayadapter);
        ListView lsEjemplo = (ListView) findViewById(R.id.semana4_listaarrayadapter);
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item);
        adaptador.add("Item 1");
        adaptador.add("Item 2");
        adaptador.add("Item 3");
        lsEjemplo.setAdapter(adaptador);
        spEjemplo.setAdapter(adaptador);

        lsEjemplo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                txtResultado.setText((String) adapterView.getItemAtPosition(i));
            }
        });

        spEjemplo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                txtResultado.setText((String) adapterView.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}