package com.example.misionticv11;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

public class semana3_login extends AppCompatActivity {

    private FirebaseAuth autCorreo;
    private String usuario = "";
    private String clave = "";
    private static final int RC_SIGN_IN = 9001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.autCorreo = FirebaseAuth.getInstance();
        FirebaseUser usuarioFirebase = this.autCorreo.getCurrentUser();
        if(usuarioFirebase!=null)
        {
            Intent menu = new Intent(this, semana3_menu.class);
            menu.putExtra("email", usuarioFirebase.getEmail());
            menu.putExtra("nombre", usuarioFirebase.getEmail());
            startActivity(menu);
        }

        setContentView(R.layout.activity_semana3_login);

        if(usuarioFirebase==null)
        {
            EditText txtUsuario = (EditText) findViewById(R.id.semana3_login_txtUsuario);
            EditText txtClave = (EditText) findViewById(R.id.semana3_login_txtClave);
            Button btnAcceder = (Button) findViewById(R.id.semana3_login_btnAcceder);
            Button btnRegistro = (Button) findViewById(R.id.semana3_login_btnRegistrarse);
            ImageButton btnGoogle = (ImageButton) findViewById(R.id.semana3_login_btnGoogle);

            btnGoogle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoogleSignInOptions gso = new
                            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestIdToken(getString(R.string.clienteweb))
                            .requestEmail()
                            .build();

                    Intent signInIntent = GoogleSignIn.getClient(v.getContext(), gso).getSignInIntent();
                    startActivityForResult(signInIntent, RC_SIGN_IN);

                }
            });

            btnRegistro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        usuario = txtUsuario.getText().toString();
                        clave = txtClave.getText().toString();

                        autCorreo.createUserWithEmailAndPassword(usuario, clave).addOnCompleteListener(
                                (Activity) view.getContext(),
                                new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful())
                                {
                                    AlertDialog.Builder msj = new AlertDialog.Builder(view.getContext());
                                    msj.setTitle("Usuario registrado");
                                    msj.setMessage("Se ha registrado el usuario correctamente.");
                                    msj.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Intent menu = new Intent(view.getContext(), semana3_menu.class);
                                            menu.putExtra("email", usuario);
                                            menu.putExtra("nombre", usuario);
                                            startActivity(menu);
                                        }
                                    });
                                    msj.create();
                                    msj.show();
                                }
                                else
                                {
                                    AlertDialog.Builder msj = new AlertDialog.Builder(view.getContext());
                                    msj.setTitle("Error");
                                    msj.setMessage("Se ha producido un error al intentar autenticar al usuario.");
                                    msj.create();
                                    msj.show();
                                }
                            }
                        });

                    }
                    catch (Exception ex)
                    {}


                }
            });

            btnAcceder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        usuario = txtUsuario.getText().toString();
                        clave = txtClave.getText().toString();


                        autCorreo.signInWithEmailAndPassword(usuario, clave).addOnCompleteListener((Activity) view.getContext(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful())
                                {
                                    Intent menu = new Intent(view.getContext(), semana3_menu.class);
                                    menu.putExtra("email", usuario);
                                    menu.putExtra("nombre", usuario);
                                    startActivity(menu);
                                }
                                else
                                {
                                    AlertDialog.Builder msj = new AlertDialog.Builder(view.getContext());
                                    msj.setTitle("Usuario no encontrado");
                                    msj.setMessage("No se ha encontrado el usuario, comuniquese con el administrador.");
                                    msj.create();
                                    msj.show();
                                }
                            }
                        });

                    }
                    catch (Exception ex)
                    {

                    }
                }
            });

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Activity activityActual = this;
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
                autCorreo.signInWithCredential(credential)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    FirebaseUser usuario = autCorreo.getCurrentUser();

                                    Intent menu = new Intent(activityActual, semana3_menu.class);
                                    menu.putExtra("email", usuario.getEmail());
                                    menu.putExtra("nombre", usuario.getDisplayName());
                                    startActivity(menu);
                                }
                            }});
            } catch (ApiException e) {
            }


        }

    }

    public static boolean camposVacios(String email, String clave)
    {
        boolean vacios = true;

        if(!email.isEmpty() && !clave.isEmpty())
            vacios = false;

        return vacios;
    }

    public static double areaTriangulo(double base, double altura)
    {
        return (base * altura)/2;
    }
}