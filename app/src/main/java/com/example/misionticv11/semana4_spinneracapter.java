package com.example.misionticv11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Spinner;

import com.example.misionticv11.adapters.EjemploSpinnerAdapter;
import com.example.misionticv11.ado.SitioADO;
import com.example.misionticv11.modelos.Sitio;

import java.util.ArrayList;

public class semana4_spinneracapter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semana4_spinneracapter);

        SitioADO db = new SitioADO(this);
        ArrayList<Sitio> sitios = db.listar();

        Spinner spEjemplo = (Spinner) findViewById(R.id.semana4_spinneradapter);
        EjemploSpinnerAdapter adaptador = new EjemploSpinnerAdapter(sitios);
        spEjemplo.setAdapter(adaptador);

    }
}