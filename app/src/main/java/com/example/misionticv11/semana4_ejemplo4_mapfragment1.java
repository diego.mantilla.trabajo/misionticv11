package com.example.misionticv11;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.misionticv11.modelos.Sitio;
import com.example.misionticv11.viewmodels.OperacionViewModel;
import com.example.misionticv11.viewmodels.SitioViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class semana4_ejemplo4_mapfragment1 extends Fragment {

    private OnMapReadyCallback callback = new OnMapReadyCallback() {

        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        @Override
        public void onMapReady(GoogleMap googleMap) {

            SitioViewModel sitiovm = ViewModelProviders.of(getActivity()).get(SitioViewModel.class);
            sitiovm.getSitio().observe(getViewLifecycleOwner(), new Observer<Sitio>() {
                @Override
                public void onChanged(Sitio sitio) {
                    LatLng sydney = new LatLng(sitio.getLatitud(), sitio.getLongitud());
                    googleMap.addMarker(new MarkerOptions().position(sydney).title(sitio.getNombre()));
                    googleMap.setMinZoomPreference(15);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                }
            });

        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_semana4_ejemplo4_mapfragment1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }
}