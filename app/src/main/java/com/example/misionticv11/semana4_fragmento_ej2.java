package com.example.misionticv11;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.misionticv11.modelos.Operacion;
import com.example.misionticv11.viewmodels.OperacionViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_fragmento_ej2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_fragmento_ej2 extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public semana4_fragmento_ej2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment semana4_fragmento_ej2.
     */
    // TODO: Rename and change types and number of parameters
    public static semana4_fragmento_ej2 newInstance(String param1, String param2) {
        semana4_fragmento_ej2 fragment = new semana4_fragmento_ej2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_semana4_fragmento_ej2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView txvResultado = (TextView) view.findViewById(R.id.semana4_ejemplo1_txvResultado);
        OperacionViewModel opvm = ViewModelProviders.of(getActivity()).get(OperacionViewModel.class);
        opvm.getOperacion().observe(getViewLifecycleOwner(), new Observer<Operacion>() {
            @Override
            public void onChanged(Operacion operacion) {
                txvResultado.setText(String.valueOf(operacion.getNumero1() + operacion.getNumero2()));
            }
        });

    }
}