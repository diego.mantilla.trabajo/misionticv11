package com.example.misionticv11;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.misionticv11.clases.Mensajes;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

public class semana3_menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semana3_menu);

        //Asociamos o conectamos los TextView con los objetos "txtNombre" y "txtEmail"
        TextView txtNombre = (TextView) findViewById(R.id.menu_txtNombre);
        TextView txtEmail = (TextView) findViewById(R.id.menu_txtEmail);

        FloatingActionButton btnMas = (FloatingActionButton) findViewById(R.id.menu_btnMas);
        btnMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Elemento que carga un layout desde la parte inferior de la pantalla.
                //showMenuInferior();

                //Elemento que pemite presentar informacion o mensajes con una duracion
                //Toast.makeText(view.getContext(), "Acaba de pulsar el boton", Toast.LENGTH_SHORT).show();

                //Elemento que permite visualizar un mensaje con una duracion corta, larga e indefinida.
                //Snackbar.make(view, "Has pulsado el boton", BaseTransientBottomBar.LENGTH_INDEFINITE).show();
            }
        });


        try {
            //Obtenenemos los datos enviados desde el intent login
            Bundle parametros = getIntent().getExtras();

            //Asignamos los datos a las variables "nombreObtenido" y "emailObtenido"
            String nombreObtenido = parametros.getString("nombre");
            String emailObtenido = parametros.getString("email");

            //Actualizamos o reemplazamos el texto que se presenta en los TextView
            txtNombre.setText(nombreObtenido);
            txtEmail.setText(emailObtenido);
        }
        catch (Exception ex)
        {
            //Actualizamos o reemplazamos el texto que se presenta en los TextView
            txtNombre.setText("<<Prueba>>");
            txtEmail.setText("<<Prueba>>");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater propiedadesMenu = getMenuInflater();
        propiedadesMenu.inflate(R.menu.mnu_principal, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Mensajes msjs = new Mensajes(this);
        //Declara el objeto intent
        Intent i = null;

        switch (item.getItemId())
        {
            //Intent implicito que me permite visualiza el contenido de una pagina web.
            case R.id.mni_acerca: i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://developer.android.com/guide/components/activities/intro-activities?hl=es"));
                break;
                //Intent implicito que me permite cargar el numero de celular.
            case R.id.mni_marcadores: i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:3102584112"));
                break;
                //Intent Explicito, que me permite cargar otro activity.
            case R.id.mni_perfil: i = new Intent(this, semana3_perfil.class);
                break;
            case R.id.mni_sitios: i = new Intent(this, semana4_versitiosActivity.class);
                break;
            case R.id.mni_cerrarsesion:
                FirebaseAuth autenticacion = FirebaseAuth.getInstance();
                autenticacion.signOut();
                onBackPressed();
                break;
        }
        if(i!=null)
            startActivity(i);

        return super.onOptionsItemSelected(item);
    }

    private void showMenuInferior()
    {
        final BottomSheetDialog bottomSheet = new BottomSheetDialog(this);
        bottomSheet.setContentView(R.layout.bottom_menu);

        bottomSheet.show();
    }

}