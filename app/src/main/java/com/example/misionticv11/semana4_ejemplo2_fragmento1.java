package com.example.misionticv11;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TwoLineListItem;

import com.example.misionticv11.adapters.EjemploListAdapter;
import com.example.misionticv11.ado.SitioADO;
import com.example.misionticv11.modelos.Sitio;
import com.example.misionticv11.viewmodels.SitioViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_ejemplo2_fragmento1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_ejemplo2_fragmento1 extends Fragment {

    public semana4_ejemplo2_fragmento1() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static semana4_ejemplo2_fragmento1 newInstance(String param1, String param2) {
        semana4_ejemplo2_fragmento1 fragment = new semana4_ejemplo2_fragmento1();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_semana4_ejemplo2_fragmento1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView lstSitios = (ListView) view.findViewById(R.id.semana4_ejemplo2_fragmento1_lstSitios);
        SitioADO sitiodb = new SitioADO(view.getContext());
        ArrayList<Sitio> sitios =  sitiodb.listar();

        EjemploListAdapter adaptador = new EjemploListAdapter(sitios);
        lstSitios.setAdapter(adaptador);

        lstSitios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TwoLineListItem item = (TwoLineListItem) view;

                SitioViewModel sitiovm = ViewModelProviders.of(getActivity()).get(SitioViewModel.class);
                Sitio s = new Sitio();
                s.setId(Integer.parseInt(item.getContentDescription().toString()));

                SitioADO sitiodb = new SitioADO(view.getContext());
                s = sitiodb.obtener(s.getId());

                sitiovm.setSitio(s);
            }
        });

    }
}