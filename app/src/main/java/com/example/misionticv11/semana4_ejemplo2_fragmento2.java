package com.example.misionticv11;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.misionticv11.ado.SitioADO;
import com.example.misionticv11.modelos.Sitio;
import com.example.misionticv11.viewmodels.SitioViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link semana4_ejemplo2_fragmento2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class semana4_ejemplo2_fragmento2 extends Fragment {

    public semana4_ejemplo2_fragmento2() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static semana4_ejemplo2_fragmento2 newInstance(String param1, String param2) {
        semana4_ejemplo2_fragmento2 fragment = new semana4_ejemplo2_fragmento2();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_semana4_ejemplo2_fragmento2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EditText txtNombre = (EditText) view.findViewById(R.id.semana4_ejemplo2_txtNombre);
        EditText txtDescripcion = (EditText) view.findViewById(R.id.semana4_ejemplo2_txtDescripcion);
        EditText txtLatitud = (EditText) view.findViewById(R.id.semana4_ejemplo2_txtLatitud);
        EditText txtLongitud = (EditText) view.findViewById(R.id.semana4_ejemplo2_txtLongitud);

        SitioViewModel sitiovm = ViewModelProviders.of(getActivity()).get(SitioViewModel.class);
        sitiovm.getSitio().observe(getViewLifecycleOwner(), new Observer<Sitio>() {
            @Override
            public void onChanged(Sitio sitio) {
                SitioADO dbsitio = new SitioADO(view.getContext());
                Sitio objeto = dbsitio.obtener(sitio.getId());

                txtNombre.setText(objeto.getNombre());
                txtDescripcion.setText(objeto.getDescripcion());
                txtLatitud.setText(String.valueOf(objeto.getLatitud()));
                txtLongitud.setText(String.valueOf(objeto.getLongitud()));

            }
        });


    }
}