package com.example.misionticv11;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.misionticv11.adapters.EjemploListAdapter;
import com.example.misionticv11.ado.SitioADO;
import com.example.misionticv11.modelos.Sitio;

import java.util.ArrayList;

public class semana4_listadapterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_semana4_listadapter);

        ListView lista = (ListView) findViewById(R.id.semana4_listadapter);
        SitioADO registros = new SitioADO(this);
        ArrayList<Sitio> sitios = registros.listar();

        EjemploListAdapter adaptador = new EjemploListAdapter(sitios);
        lista.setAdapter(adaptador);


    }
}