package com.example.misionticv11.modelos;

public class Operacion {
    private int Numero1;
    private int Numero2;

    public int getNumero1() {
        return Numero1;
    }

    public void setNumero1(int numero1) {
        Numero1 = numero1;
    }

    public int getNumero2() {
        return Numero2;
    }

    public void setNumero2(int numero2) {
        Numero2 = numero2;
    }
}
