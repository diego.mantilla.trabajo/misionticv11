package com.example.misionticv11;

import junit.framework.TestCase;

public class semana3_loginTest extends TestCase {

    public void testCamposVacios() {
        boolean resultado = semana3_login.camposVacios("dfmco999@gmail.com", "1234");

        assertEquals(false,resultado);
    }

    public void testCalcularAreaTriangulo()
    {
        double base = 15.0;
        double altura = 25.0;
        double resultado = semana3_login.areaTriangulo(15.0, 25.0);
        double resultadoEsperado = (base * altura / 2);
        assertEquals(resultadoEsperado, resultado);
    }
}