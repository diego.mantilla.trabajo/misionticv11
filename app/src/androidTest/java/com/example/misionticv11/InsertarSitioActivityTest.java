package com.example.misionticv11;

import junit.framework.TestCase;

public class InsertarSitioActivityTest extends TestCase {

    public void testValidarCamposVacios() {
        boolean resultado = InsertarSitioActivity.validarCamposVacios("Luisa","Ramirez", 10,15);
        assertEquals(true, resultado);
    }
}